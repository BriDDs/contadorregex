package contagem;

public class CsvContador {
	private int id;
	private int armazenadorLoc;
	private int armazenadorMetodo;
	private int armazenadorClasse;
	private int armazenadoMetodoDeus;
	private int armazenadorClasseDeus;
	
	

	public int getArmazenadoMetodoDeus() {
		return armazenadoMetodoDeus;
	}

	public void setArmazenadoMetodoDeus(int armazenadoMetodoDeus) {
		this.armazenadoMetodoDeus = armazenadoMetodoDeus;
	}

	public int getArmazenadorClasseDeus() {
		return armazenadorClasseDeus;
	}

	public void setArmazenadorClasseDeus(int armazenadorClasseDeus) {
		this.armazenadorClasseDeus = armazenadorClasseDeus;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getArmazenadorClasse() {
		return armazenadorClasse;
	}

	public void setArmazenadorClasse(int armazenadorClasse) {
		this.armazenadorClasse = armazenadorClasse;
	}

	public int getArmazenadorMetodo() {
		return armazenadorMetodo;
	}

	public void setArmazenadorMetodo(int armazenadorMetodo) {
		this.armazenadorMetodo = armazenadorMetodo;
	}

	public int getArmazenadorLoc() {
		return armazenadorLoc;
	}

	public void setArmazenadorLoc(int armazenadorLoc) {
		this.armazenadorLoc = armazenadorLoc;
	}

	@Override
	public String toString() {
		return " " + id +  ','  + armazenadorLoc +  ','  + armazenadorClasse +  ','  + armazenadorMetodo +  ',' 
				 + armazenadorClasseDeus +  ','  + armazenadoMetodoDeus +  '\n';

	}

}
