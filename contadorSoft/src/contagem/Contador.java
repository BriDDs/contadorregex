package contagem;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Contador {

	private String subDiretorio;
	private String padroesDeMetodos = "(^.*(public|private|protected|.*)) *(void|int|Integer|double|Double|String|string|char|long|Long|boolean|short|float|byte) .*([A-z0-9a-z]*[(].*[)]*[{])";
	private String padroesDeClasses = "(.*class) * [A-Z].*[{]";
	private String linha;
	int somarMetodoDeus = 0;
	int somarClasseDeus = 0;
	int somarClasse = 0;
	int somarLinhas = 0;
	int somarNumeroDePastas = 0;
	int somarMetodo = 0;
	static int totalPredicaoLoc = 0;
	static int totalPredicaoClasse = 0;
	static int totalPredicaoMetodo = 0;
	static int totalPredicaoClasseDeus = 0;
	static int totalPredicaoMetodoDeus = 0;
	static Predicao predicao = new Predicao();

	private static List<CsvContador> lista = new ArrayList<>();

	@SuppressWarnings("resource")
	public static void main(String[] args) throws IOException {
		Scanner s = new Scanner(System.in);
		System.out.println("Digite o diretÃ³rio do projeto");
		String caminho = s.next();

		Contador cl = new Contador();
		cl.contadorPastas(caminho);
		salvarCsv();
	}

	public void contadorPastas(String caminho) {
		File diretorio = new File(caminho);
		File[] arquivos = diretorio.listFiles();
		for (int i = 0; i < arquivos.length; i++) {
			if (arquivos[i].isDirectory()) {
				subDiretorio = arquivos[i].toString();
				try {
					contadorArquivos();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		}
	}

	public void contadorArquivos() throws IOException {
		int totalLinhasMes = 0;
		int totalClassesMes = 0;

		int predicaoLoc = 0;
		int predicaoClasse = 0;
		int predicaoMetodo = 0;
		int predicaoClasseDeus = 0;
		int predicaoMetodoDeus = 0;

		boolean statusClasseDeus = false;
		boolean statusClasse = false;
		int linhasContagem = 0;
		int classeContagem = 0;
		int somarLinhasEmBranco = 0;
		somarMetodoDeus = 0;
		somarMetodo = 0;
		somarClasseDeus = 0;
		CsvContador contadorCsv = new CsvContador();
		File diretorio = new File(subDiretorio);
		File[] arquivo = diretorio.listFiles();
		somarNumeroDePastas++;
		if (somarNumeroDePastas > 0) {

			int contadorId = Integer.parseInt(diretorio.getName());
			contadorCsv.setId(contadorId);

		}

		for (int j = 0; j < arquivo.length; j++) {
			File arq = new File(arquivo[j].getPath());
			somarLinhas = 0;
			somarClasse = 0;

			if (!(arquivo[j].isDirectory()) && arquivo[j].getPath().endsWith(".java")) {
				@SuppressWarnings("resource")
				BufferedReader arquivoLeitor = new BufferedReader(new FileReader(arq));
				while ((linha = arquivoLeitor.readLine()) != null) {
					if (linha.trim().length() == 0) {
						somarLinhasEmBranco++;
					}
					somarLinhas++;
					if (somarLinhas >= 800) {
						statusClasseDeus = true;
					}
					if (linha.matches(padroesDeMetodos) == true) {
						checagemDeus(arquivo[j].getAbsolutePath(), linha);
						somarMetodo++;
					}
					if (linha.matches(padroesDeClasses) == true) {
						somarClasse++;
						statusClasse = true;
					}
				}
				if (statusClasseDeus == true && statusClasse == true) {
					somarClasseDeus++;
				}
			}

			classeContagem = classeContagem + somarClasse;
			totalClassesMes = totalClassesMes + classeContagem;
			linhasContagem = linhasContagem + somarLinhas;
			totalLinhasMes = linhasContagem - somarLinhasEmBranco;

			predicaoLoc = totalLinhasMes / 30;
			predicaoClasse = totalClassesMes / 30;
			predicaoMetodo = somarMetodo / 30;
			predicaoClasseDeus = somarClasseDeus / 30;
			predicaoMetodoDeus = somarMetodoDeus / 30;
		}
		totalPredicaoLoc = totalPredicaoLoc + predicaoLoc;
		totalPredicaoClasse = totalPredicaoClasse + predicaoClasse;
		totalPredicaoMetodo = totalPredicaoMetodo + predicaoMetodo;
		totalPredicaoClasseDeus = totalPredicaoClasseDeus + predicaoClasseDeus;
		totalPredicaoMetodoDeus = totalPredicaoMetodoDeus + predicaoMetodoDeus;

		contadorCsv.setArmazenadorClasseDeus(somarClasseDeus);
		contadorCsv.setArmazenadoMetodoDeus(somarMetodoDeus);
		contadorCsv.setArmazenadorMetodo(somarMetodo);
		contadorCsv.setArmazenadorLoc(totalLinhasMes);
		contadorCsv.setArmazenadorClasse(classeContagem);
		lista.add(contadorCsv);

		predicao.setArmazenadorPredicaoLoc(totalPredicaoLoc);
		predicao.setArmazenadorPredicaoClasse(totalPredicaoClasse);
		predicao.setArmazenadorPredicaoMetodo(totalPredicaoMetodo);
		predicao.setArmazenadorPredicaoClasseDeus(totalPredicaoClasseDeus);
		predicao.setArmazenadorPredicaoMetodoDeus(totalPredicaoMetodoDeus);
	}

	public void checagemDeus(String linhaMetodoComparar, String linhaComparar) throws IOException {

		FileReader arquivoMetodoDeus = new FileReader(linhaMetodoComparar);
		@SuppressWarnings("resource")
		BufferedReader arquivoLeitorMetodoDeus = new BufferedReader(arquivoMetodoDeus);
		Pattern padraoLinhaEmBranco = Pattern.compile("(\\S)");
		int chaveAbertura = 1;
		int totalChave = 1;
		String linhaMetodoDeus;
		final int padraoMetodoDeus = 127;
		boolean situacaoMetodo = false;
		int linhasMetodo = 0;

		while ((linhaMetodoDeus = arquivoLeitorMetodoDeus.readLine()) != null) {

			Matcher combinarMetodoDeus = padraoLinhaEmBranco.matcher(linhaMetodoDeus);
			boolean encontrarMetodoDeus = combinarMetodoDeus.find();

			if (linhaMetodoDeus.equals(linhaComparar)) {
				situacaoMetodo = true;
				linhaComparar = null;
			}
			if (encontrarMetodoDeus == true && situacaoMetodo == true) {
				linhasMetodo++;

				if (linhaMetodoDeus.matches("(.*[}].*[{])|(.*[}])")) {
					totalChave = totalChave - chaveAbertura;
				}
				if (totalChave == -2 && linhasMetodo > padraoMetodoDeus) {
					somarMetodoDeus++;
					situacaoMetodo = false;
				}
				if (linhaMetodoDeus.matches(".*[{]")) {
					totalChave = totalChave + chaveAbertura;
				}
			}
		}

	}

	static void salvarCsv() throws IOException {
		FileWriter leitorCsv = new FileWriter("C:\\Users\\J_oa0\\Desktop\\Desktop\\contagem.csv");
		BufferedWriter arquivoLeitorCsv = new BufferedWriter(leitorCsv);
		PrintWriter leitorImprimirCsv = new PrintWriter(arquivoLeitorCsv);
		lista.sort(Comparator.comparing(CsvContador::getId));

		System.out.println(" PREDICAO LOC " + totalPredicaoLoc);
		System.out.println(" PREDICAO CLASSE " + totalPredicaoClasse);
		System.out.println(" PREDICAO METODO " + totalPredicaoMetodo);
		System.out.println(" PREDICAO CLASSE DEUS " + totalPredicaoClasseDeus);
		System.out.println(" PREDICAO METODODEUS " + totalPredicaoMetodoDeus);

		leitorImprimirCsv.print((" " + " Mes " + ',' + "Loc" + ',' + "Classe" + ',' + "Metodo" + ',' + " ClasseDeus "
				+ ',' + " MetodoDeus " + '\n'));

		for (CsvContador csvContador : lista) {
			System.out.println(csvContador.toString());
			leitorImprimirCsv.print(csvContador.toString());
		}
		leitorImprimirCsv.print(predicao.toString());
		leitorImprimirCsv.flush();
		leitorImprimirCsv.close();
	}
}
