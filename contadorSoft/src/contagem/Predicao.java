package contagem;

public class Predicao {
	private int idPredicao = 28;
	private int armazenadorPredicaoLoc;
	private int armazenadorPredicaoClasse;
	private int armazenadorPredicaoMetodo;
	private int armazenadorPredicaoMetodoDeus;
	private int armazenadorPredicaoClasseDeus;

	public int getIdPredicao() {
		return idPredicao;
	}

	public void setIdPredicao(int idPredicao) {
		this.idPredicao = idPredicao;
	}

	public int getArmazenadorPredicaoLoc() {
		return armazenadorPredicaoLoc;
	}

	public void setArmazenadorPredicaoLoc(int armazenadorPredicaoLoc) {
		this.armazenadorPredicaoLoc = armazenadorPredicaoLoc;
	}

	public int getArmazenadorPredicaoClasse() {
		return armazenadorPredicaoClasse;
	}

	public void setArmazenadorPredicaoClasse(int armazenadorPredicaoClasse) {
		this.armazenadorPredicaoClasse = armazenadorPredicaoClasse;
	}

	public int getArmazenadorPredicaoMetodo() {
		return armazenadorPredicaoMetodo;
	}

	public void setArmazenadorPredicaoMetodo(int armazenadorPredicaoMetodo) {
		this.armazenadorPredicaoMetodo = armazenadorPredicaoMetodo;
	}

	public int getArmazenadorPredicaoMetodoDeus() {
		return armazenadorPredicaoMetodoDeus;
	}

	public void setArmazenadorPredicaoMetodoDeus(int armazenadorPredicaoMetodoDeus) {
		this.armazenadorPredicaoMetodoDeus = armazenadorPredicaoMetodoDeus;
	}

	public int getArmazenadorPredicaoClasseDeus() {
		return armazenadorPredicaoClasseDeus;
	}

	public void setArmazenadorPredicaoClasseDeus(int armazenadorPredicaoClasseDeus) {
		this.armazenadorPredicaoClasseDeus = armazenadorPredicaoClasseDeus;
	}

	@Override
	public String toString() {
		return " " + idPredicao +  ','  + armazenadorPredicaoLoc +  ','  + armazenadorPredicaoClasse +  ','  + armazenadorPredicaoMetodo +  ',' 
				 + armazenadorPredicaoClasseDeus +  ','  + armazenadorPredicaoMetodoDeus +  '\n';
	}

}
